package ru.geekbrains.instagram_client.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import ru.geekbrains.instagram_client.R;
import ru.geekbrains.instagram_client.data.Photo;
import ru.geekbrains.instagram_client.fragments.SinglePhotoFragment;

public class SinglePhotoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_photo);

        if (savedInstanceState == null) {
            Photo photo = getIntent().getParcelableExtra(getString(R.string.photo_key));
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fl_activity_single_photo,
                            SinglePhotoFragment.newInstance(this, photo))
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }
}
