package ru.geekbrains.instagram_client.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.geekbrains.instagram_client.R;
import ru.geekbrains.instagram_client.adapters.MainViewPagerAdapter;
import ru.geekbrains.instagram_client.data.Photo;
import ru.geekbrains.instagram_client.fragments.ChangeThemeFragment;
import ru.geekbrains.instagram_client.fragments.mainTabFragments.FavoritesFragment;
import ru.geekbrains.instagram_client.fragments.mainTabFragments.HomeFragment;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnActivityCallback,
        ChangeThemeFragment.OnActivityCallback {
    private static final int DRAWER_GRAVITY = GravityCompat.START;
    private static final int FAVORITE_TAB_POSITION = 1;
    private static final int HOME_TAB_POSITION = 0;

    @BindView(R.id.dl_main_activity_start)
    DrawerLayout startDrawerLayout;
    @BindView(R.id.nv_main_activity)
    NavigationView navigationView;
    @BindView(R.id.main_activity_toolbar)
    Toolbar toolbar;
    @BindView(R.id.tl_main_activity)
    TabLayout tabLayout;
    @BindView(R.id.vp_main_activity_viewpager)
    ViewPager viewPager;


    private boolean isNavigationViewOpen;
    private int selectedThemeId;
    private int selectedNavigationItemId;
    private MainViewPagerAdapter mainViewPagerAdapter;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSettingsFromSharedPref();
        setTheme(selectedThemeId);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        restoreDrawOpenState();
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(getString(R.string.nav_state_shared_pref_key), isNavigationViewOpen);
        editor.putInt(getString(R.string.app_theme_shared_pref_key), selectedThemeId);
        editor.putInt(getString(R.string.nav_selected_item_id_shared_pref_key), selectedNavigationItemId);
        editor.apply();
    }

    @Override
    public void onBackPressed() {
        if (startDrawerLayout.isDrawerOpen(DRAWER_GRAVITY)) {
            startDrawerLayout.closeDrawer(DRAWER_GRAVITY);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void changeThemeButtonClick(int themeId) {
        selectedThemeId = themeId;
        setTheme(selectedThemeId);
        recreate();
    }


    @Override
    public void openSinglePhoto(Photo photo) {
        Intent intent = new Intent(MainActivity.this, SinglePhotoActivity.class);
        intent.putExtra(getString(R.string.photo_key), photo);
        startActivity(intent);
    }

    @Override
    public void changeFavoriteStatus(Photo photo) {
        if (!photo.isFavorite()) {
            addPhotoToFavorite(photo);

        } else {
            //delete photo
        }

    }


    private void addPhotoToFavorite(Photo photo) {
        FavoritesFragment favoritesFragment = (FavoritesFragment)
                mainViewPagerAdapter.getItem(FAVORITE_TAB_POSITION);
        if (favoritesFragment != null) {
            favoritesFragment.addPhoto(photo);
        }
    }

    private void getSettingsFromSharedPref() {
        sharedPreferences = getPreferences(MODE_PRIVATE);
        Resources resources = getResources();
        isNavigationViewOpen = sharedPreferences.getBoolean(getString(R.string.nav_state_shared_pref_key),
                resources.getBoolean(R.bool.main_activity_nav_drawer_default));
        selectedNavigationItemId = sharedPreferences.getInt(
                getString(R.string.nav_selected_item_id_shared_pref_key), R.id.main_menu_nav_home);
        selectedThemeId = sharedPreferences.getInt(getString(R.string.app_theme_shared_pref_key),
                R.style.AppBaseTheme_AppFirstTheme);
    }

    private void initUI() {
        initToolbar();
        initTabLayout();
        initViewPager();
        initDrawerLayout();
        initNavigationView();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initTabLayout() {
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initViewPager() {
        mainViewPagerAdapter = createMainViewPagerAdapter(HOME_TAB_POSITION, FAVORITE_TAB_POSITION);
        viewPager.setAdapter(mainViewPagerAdapter);
    }

    private void initDrawerLayout() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, startDrawerLayout, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        startDrawerLayout.addDrawerListener(toggle);
        startDrawerLayout.addDrawerListener(createDrawerListener());
        toggle.syncState();
    }

    private void initNavigationView() {
        navigationView.setNavigationItemSelectedListener(createNavigationListener());
        navigationView.setCheckedItem(selectedNavigationItemId);

    }

    @NonNull
    private MainViewPagerAdapter createMainViewPagerAdapter(int homeTabPos, int favoriteTabPos) {
        MainViewPagerAdapter mainViewPagerAdapter =
                new MainViewPagerAdapter(getSupportFragmentManager());

        String titleHomeFragment = getString(R.string.main_activity_tab_home_text);
        Fragment homeFragment = HomeFragment.newInstance(this, null);

        String titleFavoriteHome = getString(R.string.main_activity_tab_favorite_text);
        Fragment favoriteFragment = FavoritesFragment.newInstance(this, null);


        if (homeTabPos < favoriteTabPos) {
            mainViewPagerAdapter.addFragment(titleHomeFragment, homeFragment);
            mainViewPagerAdapter.addFragment(titleFavoriteHome, favoriteFragment);
        } else {
            mainViewPagerAdapter.addFragment(titleFavoriteHome, favoriteFragment);
            mainViewPagerAdapter.addFragment(titleHomeFragment, homeFragment);
        }
        return mainViewPagerAdapter;
    }

       /*private void showChangeThemeFragment(FragmentManager fragmentManager, Fragment containerFragment) {
        if (containerFragment instanceof ChangeThemeFragment) {
            return;
        }
        Fragment changeThemeFragment = fragmentManager.findFragmentByTag(ChangeThemeFragment.TAG);
        fragmentManager.beginTransaction()
                .replace(R.id.fl_main_activity_main_container,
                        (changeThemeFragment == null)
                                ? ChangeThemeFragment.newInstance()
                                : containerFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }*/

    private void restoreDrawOpenState() {
        if (isNavigationViewOpen) {
            startDrawerLayout.openDrawer(DRAWER_GRAVITY);
        } else {
            startDrawerLayout.closeDrawer(DRAWER_GRAVITY);
        }

    }

    @NonNull
    private NavigationView.OnNavigationItemSelectedListener createNavigationListener() {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (startDrawerLayout.isDrawerOpen(DRAWER_GRAVITY)) {
                    startDrawerLayout.closeDrawer(DRAWER_GRAVITY);
                }

                int itemId = item.getItemId();
                if (itemId == selectedNavigationItemId) {
                    return true;
                }

                FragmentManager fragmentManager = getSupportFragmentManager();
                Fragment containerFragment = fragmentManager
                        .findFragmentById(R.id.fl_main_activity_main_container);

                if (itemId == R.id.main_menu_nav_change_theme) {
                    // showChangeThemeFragment(fragmentManager, containerFragment);
                } else {
                    //showMainFragment(fragmentManager, containerFragment);
                }

                selectedNavigationItemId = item.getItemId();
                return true;
            }
        };
    }

    @NonNull
    private DrawerLayout.SimpleDrawerListener createDrawerListener() {
        return new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                isNavigationViewOpen = true;
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                isNavigationViewOpen = false;
            }
        };
    }

}
