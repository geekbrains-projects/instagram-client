package ru.geekbrains.instagram_client.fragments.mainTabFragments;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.geekbrains.instagram_client.R;
import ru.geekbrains.instagram_client.adapters.PhotosAdapter;
import ru.geekbrains.instagram_client.data.Photo;
import ru.geekbrains.instagram_client.utils.Utils;
import ru.geekbrains.instagram_client.widgets.SpacingItemDecoration;

public class FavoritesFragment extends Fragment {
    @BindView(R.id.rv_fragment_favorites)
    RecyclerView photosRecycler;

    private PhotosAdapter photosAdapter;
    private Unbinder unbinder;

    public FavoritesFragment() {

    }

    @NonNull
    public static FavoritesFragment newInstance(Context context, @Nullable Bundle bundle) {
        FavoritesFragment fragment = new FavoritesFragment();
        Bundle args = new Bundle();
        args.putBundle(context.getString(R.string.fragment_arguments_key), bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites_data, container, false);
        unbinder = ButterKnife.bind(this, view);
        initUi();
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void addPhoto(Photo photo) {
        photosAdapter.addPhoto(photo);
    }

    private void initUi() {
        initRecycler(getContext());
    }

    private void initRecycler(Context context) {
        Resources resources = getResources();
        int spanCount = Utils.calculateSpanCount(context, resources
                .getInteger(R.integer.fragment_main_grid_span_scaling_factor));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, spanCount);
        photosRecycler.setLayoutManager(gridLayoutManager);
        photosRecycler.addItemDecoration(
                new SpacingItemDecoration(
                        spanCount,
                        resources.getInteger(R.integer.main_fragment_rv_item_decorator_spacing),
                        true));
        photosAdapter = new PhotosAdapter(new ArrayList<Photo>(), getPhotoAdapterCallback());
        photosRecycler.setAdapter(photosAdapter);
    }

    private PhotosAdapter.PhotoAdapterCallback getPhotoAdapterCallback() {
        return new PhotosAdapter.PhotoAdapterCallback() {
            @Override
            public void onPhotoClick(Photo photo) {

            }

            @Override
            public void onItemLongClick(Photo photo) {

            }

            @Override
            public void onFavoriteStatusClick(Photo photo) {

            }
        };
    }


}
