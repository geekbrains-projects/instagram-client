package ru.geekbrains.instagram_client.fragments.mainTabFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.geekbrains.instagram_client.R;
import ru.geekbrains.instagram_client.data.Photo;
import ru.geekbrains.instagram_client.fragments.homeHostFragments.CommonDataFragment;
import ru.geekbrains.instagram_client.fragments.homeHostFragments.DatabaseDataFragment;
import ru.geekbrains.instagram_client.fragments.homeHostFragments.NetworkDataFragment;

public class HomeFragment extends Fragment implements CommonDataFragment.OnFragmentCallback {
    public interface OnActivityCallback {
        void openSinglePhoto(Photo photo);

        void changeFavoriteStatus(Photo photo);
    }

    @BindView(R.id.bnv_fragment_home_navigation)
    BottomNavigationView bottomNavigationView;

    private final BottomNavigationView.OnNavigationItemSelectedListener botNavSelectedListener
            = getNavigationSelectedListener();

    private NetworkDataFragment networkDataFragment;
    private DatabaseDataFragment databaseDataFragment;
    private CommonDataFragment commonDataFragment;

    private int botNavSelectedItemId;
    private boolean isBotNavSelectedBefore;
    private Unbinder unbinder;
    private OnActivityCallback onActivityCallback;


    public HomeFragment() {

    }

    @NonNull
    public static HomeFragment newInstance(Context context, @Nullable Bundle bundle) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putBundle(context.getString(R.string.fragment_arguments_key), bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        initFragments(getContext());
        initUi();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HomeFragment.OnActivityCallback) {
            onActivityCallback = (HomeFragment.OnActivityCallback) context;
        } else {
            throw new RuntimeException(getString(R.string.error_implement_on_activity_callback) +
                    HomeFragment.class.toString());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = PreferenceManager
                .getDefaultSharedPreferences(getContext()).edit();
        editor.putInt(getString(R.string.bot_nav_view_selected_item_id_key),
                botNavSelectedItemId);
        editor.apply();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onActivityCallback = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void openSinglePhoto(Photo photo) {
        onActivityCallback.openSinglePhoto(photo);
    }

    @Override
    public void changeFavoriteStatus(Photo photo) {
        onActivityCallback.changeFavoriteStatus(photo);
    }


    private void initFragments(Context context) {
        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment;

        fragment = fragmentManager.findFragmentByTag(DatabaseDataFragment.TAG);
        databaseDataFragment = (DatabaseDataFragment) ((fragment == null)
                ? DatabaseDataFragment.newInstance(context, null)
                : fragment);

        fragment = fragmentManager.findFragmentByTag(DatabaseDataFragment.TAG);
        networkDataFragment = (fragment == null)
                ? NetworkDataFragment.newInstance(context, null)
                : (NetworkDataFragment) fragment;

        fragment = fragmentManager.findFragmentByTag(CommonDataFragment.TAG);
        commonDataFragment = (CommonDataFragment) ((fragment == null)
                ? CommonDataFragment.newInstance(context, null)
                : fragment);
    }

    private void initUi() {
        initBottomNavigation(getContext());
    }

    private void initBottomNavigation(Context context) {
        bottomNavigationView.setOnNavigationItemSelectedListener(botNavSelectedListener);
        bottomNavigationView.setOnNavigationItemReselectedListener(getNavigationReselectedListener());

        int defaultValue = R.id.menu_fragment_home_common;
        botNavSelectedItemId = PreferenceManager.getDefaultSharedPreferences(context)
                .getInt(context.getString(R.string.bot_nav_view_selected_item_id_key), defaultValue);
        bottomNavigationView.setSelectedItemId(botNavSelectedItemId);
    }

    private BottomNavigationView.OnNavigationItemReselectedListener getNavigationReselectedListener() {
        return new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {
                if (!isBotNavSelectedBefore) {
                    botNavSelectedListener.onNavigationItemSelected(item);
                    isBotNavSelectedBefore = !isBotNavSelectedBefore;
                }
            }
        };
    }

    private BottomNavigationView.OnNavigationItemSelectedListener getNavigationSelectedListener() {
        return new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                switch (id) {
                    case R.id.menu_fragment_home_network: {
                        transaction.replace(R.id.fl_fragment_home_main_container,
                                networkDataFragment, NetworkDataFragment.TAG);
                        break;
                    }
                    case R.id.menu_fragment_home_database: {
                        transaction.replace(R.id.fl_fragment_home_main_container,
                                databaseDataFragment, DatabaseDataFragment.TAG);
                        break;
                    }
                    case R.id.menu_fragment_home_common: {
                        transaction.replace(R.id.fl_fragment_home_main_container,
                                commonDataFragment, CommonDataFragment.TAG);
                        break;
                    }
                    default: {
                        throw new RuntimeException(getString(R.string.error_unknown_nav_item_id) +
                                HomeFragment.class.toString());
                    }
                }
                transaction
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();

                botNavSelectedItemId = id;
                return true;
            }
        };
    }


}
