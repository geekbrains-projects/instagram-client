package ru.geekbrains.instagram_client.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.geekbrains.instagram_client.R;


public class ChangeThemeFragment extends Fragment {
    public interface OnActivityCallback {
        void changeThemeButtonClick(int themeId);
    }

    private OnActivityCallback onActivityCallback;
    private Unbinder unbinder;

    public ChangeThemeFragment() {
    }

    @NonNull
    public static ChangeThemeFragment newInstance(Context context, @Nullable Bundle bundle) {
        ChangeThemeFragment fragment =  new ChangeThemeFragment();
        Bundle args = new Bundle();
        args.putBundle(context.getString(R.string.fragment_arguments_key), bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_theme, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnActivityCallback) {
            onActivityCallback = (OnActivityCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + getString(R.string.error_implement_on_activity_callback));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onActivityCallback = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.bt_fragment_change_theme_first_theme,
            R.id.bt_fragment_change_theme_second_theme,
            R.id.bt_fragment_change_theme_third_theme})
    void onButtonsClick(View view) {
        int themeId;
        switch (view.getId()) {
            case R.id.bt_fragment_change_theme_second_theme: {
                themeId = R.style.AppBaseTheme_AppSecondTheme;
                break;
            }
            case R.id.bt_fragment_change_theme_third_theme: {
                themeId = R.style.AppBaseTheme_AppThirdTheme;
                break;
            }
            default: {
                themeId = R.style.AppBaseTheme_AppFirstTheme;
                break;
            }
        }
        onActivityCallback.changeThemeButtonClick(themeId);
    }
}
