package ru.geekbrains.instagram_client.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.geekbrains.instagram_client.R;
import ru.geekbrains.instagram_client.data.Photo;


public class SinglePhotoFragment extends Fragment {
    @BindView(R.id.iv_single_photo_fragment_photo)
    ImageView photoImageView;

    private Photo photo;
    private Unbinder unbinder;

    public SinglePhotoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            photo = getArguments().getParcelable(getString(R.string.photo_key));
        } else {
            throw new RuntimeException(getString(R.string.error_fragment_args)
                    + SinglePhotoFragment.class.getName());
        }
    }

    @NonNull
    public static SinglePhotoFragment newInstance(Context context, Photo photo) {
        SinglePhotoFragment fragment = new SinglePhotoFragment();
        Bundle args = new Bundle();
        args.putParcelable(context.getString(R.string.photo_key), photo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_single_photo, container, false);
        unbinder = ButterKnife.bind(this, view);
        Picasso
                .with(getContext())
                .load(photo.getFileUri())
                .into(photoImageView);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
