package ru.geekbrains.instagram_client.fragments.homeHostFragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.FileProvider;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.geekbrains.instagram_client.R;
import ru.geekbrains.instagram_client.adapters.PhotosAdapter;
import ru.geekbrains.instagram_client.asynTaskLoaders.StoragePhotosAsyncTaskLoader;
import ru.geekbrains.instagram_client.data.Photo;
import ru.geekbrains.instagram_client.utils.FilesUtils;
import ru.geekbrains.instagram_client.utils.Utils;
import ru.geekbrains.instagram_client.widgets.SpacingItemDecoration;

import static android.app.Activity.RESULT_OK;


public class CommonDataFragment extends Fragment {
    public static final String TAG = CommonDataFragment.class.getSimpleName();
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int EXTERNAL_STORAGE_LOADER_ID = 11;
    private static final int REQUEST_PERMISSION = 234;


    public interface OnFragmentCallback {
        void openSinglePhoto(Photo photo);

        void changeFavoriteStatus(Photo photo);
    }

    @BindView(R.id.cl_fragment_main)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.rv_fragment_common_data)
    RecyclerView photosRecyclerView;

    private final LoaderManager.LoaderCallbacks<List<Photo>> photoLoaderCallback =
            createLoaderCallback();

    private boolean storagePermissionGranted;
    private OnFragmentCallback onFragmentCallback;
    private Unbinder unbinder;
    private PhotosAdapter photosAdapter;
    private File currentFile;

    public CommonDataFragment() {

    }

    @NonNull
    public static Fragment newInstance(Context context, @Nullable Bundle bundle) {
        CommonDataFragment fragment = new CommonDataFragment();
        Bundle args = new Bundle();
        args.putBundle(context.getString(R.string.fragment_arguments_key), bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onAttachToParentFragment(getParentFragment());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.getSupportLoaderManager().initLoader(EXTERNAL_STORAGE_LOADER_ID,
                    getStorageLoaderBundle(), photoLoaderCallback);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_common_data, container, false);
        unbinder = ButterKnife.bind(this, view);
        initUi();

        if (savedInstanceState == null) {
            checkWriteExternalPerm();
        } else {
            storagePermissionGranted = savedInstanceState
                    .getBoolean(getString(R.string.write_external_granted_storage_key));
            restorePhotosRecycler(savedInstanceState);
        }

        Objects.requireNonNull(getActivity()).getSupportLoaderManager()
                .initLoader(EXTERNAL_STORAGE_LOADER_ID, getStorageLoaderBundle(),
                        photoLoaderCallback);
        return view;
    }

    private void onAttachToParentFragment(Fragment fragment) {
        if (fragment instanceof OnFragmentCallback) {
            onFragmentCallback = (OnFragmentCallback) fragment;
        } else {
            throw new RuntimeException(getString(R.string.error_implement_fragment_callback) + TAG);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(getString(R.string.write_external_granted_storage_key),
                storagePermissionGranted);
        outState.putParcelable(getString(R.string.saved_recycler_position_key),
                photosRecyclerView.getLayoutManager().onSaveInstanceState());

    }

    @Override
    public void onDetach() {
        super.onDetach();
        onFragmentCallback = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                addPhoto(new Photo(Uri.fromFile(currentFile)));
            } else {
                currentFile.delete();
            }
            currentFile = null;
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION) {
            storagePermissionGranted = (grantResults[0] == PackageManager.PERMISSION_GRANTED);
        }
    }

    @OnClick(R.id.fab_fragment_common_add)
    public void onClickAddPhotoButton() {
        if (storagePermissionGranted) {
            getPhotoFromCamera();
        } else {
            showMessage(getString(R.string.no_write_external_permission));
        }
    }

    private void getPhotoFromCamera() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return;
        }

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            currentFile = FilesUtils.createPhotoFile(
                    getString(R.string.main_activity_data_format),
                    getString(R.string.jpeg),
                    getString(R.string.jpg),
                    activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES));

            if (currentFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        FileProvider.getUriForFile(activity, activity.getPackageName(), currentFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private void checkWriteExternalPerm() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                Objects.requireNonNull(getActivity())
                        .checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION);
        } else {
            storagePermissionGranted = true;
        }
    }

    private void initUi() {
        initPhotosRecycler(getContext());
    }

    private void initPhotosRecycler(Context context) {
        Resources resources = getResources();
        int spanCount = Utils.calculateSpanCount(context, resources
                .getInteger(R.integer.fragment_main_grid_span_scaling_factor));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, spanCount);
        photosRecyclerView.setLayoutManager(gridLayoutManager);
        photosRecyclerView.addItemDecoration(
                new SpacingItemDecoration(
                        spanCount,
                        resources.getInteger(R.integer.main_fragment_rv_item_decorator_spacing),
                        true));

    }

    private void restorePhotosRecycler(Bundle savedInstanceState) {
        Parcelable savedPhotosRecyclerPos = savedInstanceState.getParcelable(
                getString(R.string.saved_recycler_position_key));
        photosRecyclerView.getLayoutManager().onRestoreInstanceState(savedPhotosRecyclerPos);
    }

    private void showMessage(String message) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @NonNull
    private LoaderManager.LoaderCallbacks<List<Photo>> createLoaderCallback() {
        return new LoaderManager.LoaderCallbacks<List<Photo>>() {
            @NonNull
            @Override
            public Loader<List<Photo>> onCreateLoader(int id, @Nullable Bundle args) {
                return new StoragePhotosAsyncTaskLoader(getActivity(), args);
            }

            @Override
            public void onLoadFinished(@NonNull Loader<List<Photo>> loader, List<Photo> data) {
                if (data != null) {
                    photosAdapter = new PhotosAdapter(data, createPhotoAdapterCallback());
                    photosRecyclerView.setAdapter(photosAdapter);
                }
            }

            @Override
            public void onLoaderReset(@NonNull Loader<List<Photo>> loader) {

            }
        };
    }

    @Nullable
    private Bundle getStorageLoaderBundle() {
        Context context = getContext();
        if (context == null) {
            return null;
        }

        Bundle bundle = null;
        File directory = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (directory != null && directory.exists()) {
            bundle = new Bundle();
            bundle.putString(getString(R.string.storage_parent_key), directory.getPath());
        }
        return bundle;
    }

    private void addPhoto(Photo photo) {
        int addPosition = photosAdapter.addPhoto(photo);
        photosRecyclerView.scrollToPosition(addPosition);
        showMessage(getString(R.string.photo_add));
    }

    private void deletePhoto(Photo photo) {
        if (storagePermissionGranted) {
            FilesUtils.deletePhoto(photo);
            photosAdapter.deletePhoto(photo);
        } else {
            showMessage(getString(R.string.error_delete_photo_no_permis));
        }
    }

    @NonNull
    private PhotosAdapter.PhotoAdapterCallback createPhotoAdapterCallback() {
        return new PhotosAdapter.PhotoAdapterCallback() {
            @Override
            public void onPhotoClick(Photo photo) {
                onFragmentCallback.openSinglePhoto(photo);
            }

            @Override
            public void onItemLongClick(Photo photo) {
                showDeletePhotoDialog(photo);
            }

            @Override
            public void onFavoriteStatusClick(Photo photo) {
                onFragmentCallback.changeFavoriteStatus(photo);
                showMessage(getString(photo.isFavorite()
                        ? R.string.photos_deleted_from_favorite
                        : R.string.photos_added_to_favorite));
            }
        };
    }

    private void showDeletePhotoDialog(final Photo photo) {
        new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                .setMessage(R.string.dialog_delete_photo)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deletePhoto(photo);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                })
                .create()
                .show();

    }
}


