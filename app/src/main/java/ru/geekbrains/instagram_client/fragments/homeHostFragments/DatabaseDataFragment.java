package ru.geekbrains.instagram_client.fragments.homeHostFragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.geekbrains.instagram_client.R;


public class DatabaseDataFragment extends Fragment {
    public static final String TAG = DatabaseDataFragment.class.toString();

    public DatabaseDataFragment() {

    }

    @NonNull
    public static Fragment newInstance(Context context, @Nullable Bundle bundle) {
        DatabaseDataFragment fragment = new DatabaseDataFragment();
        Bundle args = new Bundle();
        args.putBundle(context.getString(R.string.fragment_arguments_key), bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_database_data, container, false);
    }

}
