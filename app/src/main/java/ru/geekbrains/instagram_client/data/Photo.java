package ru.geekbrains.instagram_client.data;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class Photo implements Parcelable {
    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    private final Uri fileUri;
    private boolean isFavorite;

    public Photo(Uri fileUri) {
        this.fileUri = fileUri;
        isFavorite = false;
    }

    protected Photo(Parcel in) {
        fileUri = in.readParcelable(Uri.class.getClassLoader());
    }

    public Uri getFileUri() {
        return fileUri;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(fileUri, flags);
    }
}
