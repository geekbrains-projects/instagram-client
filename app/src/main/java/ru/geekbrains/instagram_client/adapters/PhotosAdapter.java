package ru.geekbrains.instagram_client.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.geekbrains.instagram_client.R;
import ru.geekbrains.instagram_client.data.Photo;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.PhotoViewHolder> {


    public interface PhotoAdapterCallback {
        void onPhotoClick(Photo photo);

        void onItemLongClick(Photo photo);

        void onFavoriteStatusClick(Photo photo);
    }

    private final PhotoAdapterCallback photoAdapterCallback;
    private final List<Photo> photoList;

    public PhotosAdapter(List<Photo> photoList, PhotoAdapterCallback photoAdapterCallback) {
        this.photoList = photoList;
        this.photoAdapterCallback = photoAdapterCallback;
    }

    @NonNull
    @Override
    public PhotosAdapter.PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.photo_card, parent, false);

        return new PhotoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotosAdapter.PhotoViewHolder holder, int position) {
        holder.bind(photoList.get(position));
    }

    @Override
    public int getItemCount() {
        return photoList == null ? 0 : photoList.size();
    }

    public int addPhoto(Photo photo) {
        photoList.add(photo);
        int insertedPosition = photoList.size() - 1;
        notifyItemInserted(insertedPosition);
        return insertedPosition;
    }

    public void deletePhoto(Photo photo) {
        int position = photoList.indexOf(photo);
        photoList.remove(position);
        notifyItemRemoved(position);
    }

    public void setData(List<Photo> photos) {
        clearData();
        for (Photo photo : photos) {
            addPhoto(photo);
        }
    }

    private void clearData() {
        for (Photo photo : photoList) {
            deletePhoto(photo);
        }
        photoList.clear();
    }

    class PhotoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_photo_card_photo)
        ImageView photoImage;

        PhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            photoImage.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    photoAdapterCallback.onItemLongClick(photoList.get(getAdapterPosition()));
                    return true;
                }
            });
        }

        void bind(final Photo photo) {
            Picasso
                    .with(photoImage.getContext())
                    .load(photo.getFileUri())
                    .error(R.drawable.ic_instagram)
                    .resizeDimen(R.dimen.cv_photo_card_width, R.dimen.cv_photo_card_height)
                    .into(photoImage);
        }

        @OnClick({R.id.iv_photo_card_photo})
        void onPhotoClick() {
            photoAdapterCallback.onPhotoClick(photoList.get(getAdapterPosition()));
        }

        @OnClick({R.id.iv_photo_card_favorite_status})
        void onFavoriteStatusClick() {
            photoAdapterCallback.onFavoriteStatusClick(photoList.get(getAdapterPosition()));
        }
    }

}
