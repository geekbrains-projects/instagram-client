package ru.geekbrains.instagram_client.utils;

import android.support.annotation.Nullable;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.geekbrains.instagram_client.data.Photo;

public class FilesUtils {
    @Nullable
    public static File createPhotoFile(String pattern, String baseName, String fileExtension,
                                       File storageDirectory) {
        File file = null;
        String timeStamp = new SimpleDateFormat(pattern, Locale.getDefault()).format(new Date());
        String imageFileName = baseName + timeStamp;
        try {
            file = File.createTempFile(imageFileName, fileExtension, storageDirectory);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    @Nullable
    public static List<File> getPhotos(String parentDirName) {
        List<File> storageFiles = null;
        File parentDirectory = new File(parentDirName);
        if (parentDirectory.isDirectory()) {
            storageFiles = Arrays.asList(parentDirectory.listFiles());
        }
        return storageFiles;
    }


    public static void deletePhoto(Photo photo) {
        File file = new File(photo.getFileUri().getPath());
        if (file.exists()) {
            file.delete();
        }

    }


}
