package ru.geekbrains.instagram_client.utils;

import android.content.Context;
import android.util.DisplayMetrics;

import ru.geekbrains.instagram_client.R;

public class Utils {

    public static int calculateSpanCount(Context context, int scalingFactor) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int minNoOfColumns = context.getResources().getInteger(R.integer.min_count_grid_span);
        int noOfColumns = (int) (dpWidth / scalingFactor);
        if (noOfColumns < minNoOfColumns) {
            noOfColumns = minNoOfColumns;
        }
        return noOfColumns;
    }
}
