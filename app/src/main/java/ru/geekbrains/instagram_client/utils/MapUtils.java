package ru.geekbrains.instagram_client.utils;

import android.net.Uri;
import android.support.annotation.NonNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ru.geekbrains.instagram_client.data.Photo;

public class MapUtils {

    @NonNull
    public static List<Photo> mapFilesToPhoto(List<File> files) {
        List<Photo> photoList = new ArrayList<>();
        for (int i = 0; i < files.size(); i++) {
            photoList.add(new Photo(Uri.fromFile(files.get(i))));
        }
        return photoList;
    }
}
