package ru.geekbrains.instagram_client.asynTaskLoaders;

import android.content.Context;
import android.os.Bundle;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ru.geekbrains.instagram_client.R;
import ru.geekbrains.instagram_client.data.Photo;
import ru.geekbrains.instagram_client.utils.FilesUtils;
import ru.geekbrains.instagram_client.utils.MapUtils;


public class StoragePhotosAsyncTaskLoader extends BaseAsyncTaskLoader<List<Photo>> {
    private String parentDirectoryPath;

    public StoragePhotosAsyncTaskLoader(Context context, Bundle args) {
        super(context);
        if (args != null) {
            parentDirectoryPath = args.getString(context.getString(R.string.storage_parent_key));
        }
    }

    @Override
    protected void onStartLoading() {
        if (parentDirectoryPath != null) {
            startLoad();
        }
    }

    @Override
    public List<Photo> loadInBackground() {
        List<File> filesList = FilesUtils.getPhotos(parentDirectoryPath);
        List<Photo> photoList = new ArrayList<>();
        if (filesList != null) {
            photoList = MapUtils.mapFilesToPhoto(filesList);
        }
        return photoList;
    }
}
